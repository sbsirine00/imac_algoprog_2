#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void swap(int* a, int* b)
{
    int t = *a;
    *a = *b;
    *b = t;
}

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	
	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0; // effectives sizes

    int pivot = greaterArray[size]; // pivot
        int i = (lowerArray[size] - 1);

    // split on divise le tableau dans lower et greater
    for (int j = lowerArray[size]; j <= greaterArray[size] - 1; j++)
        {
            // If current element is smaller than the pivot
            if (toSort[j] < pivot)
            {
                i++; // increment index of smaller element
                swap(&toSort[i], &toSort[j]);
            }
        }
        swap(&toSort[i + 1], &toSort[size]);
       // return (i + 1);
	
    // recursiv sort of lowerArray and greaterArray on rappel la fct recursivquicksort
        if (lowerArray[size] < greaterArray[size])
            {
                /* pi is partitioning index, arr[p] is now
                at right place */
                int pi = recursivQuickSort(Array& toSort, int size);

                // Separately sort elements before
                // partition and after partition
                quickSort(toSort, lowerArray[size], pi - 1);
                quickSort(toSort, pi + 1, greaterArray[size]);
            }

    // merge on remet dans le tableau


}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
