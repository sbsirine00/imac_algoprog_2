#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    nodeIndex *2 + 1;
    return 0;
}

int Heap::rightChild(int nodeIndex)
{
    nodeIndex *2 + 2;
    return 0;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;

        value = value + 1;
        (*this)[value - 1]= i;
        heapify(this->get(i), value, value - 1);
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;

    // Find parent
        int parent = (i_max - 1) / 2;

        if (this->get(i_max) > 0) {
            // For Max-Heap
            // If current node is greater than its parent
            // Swap both of them and call heapify again
            // for the parent
            if (this->get(i_max) > this->get(parent)) {
                swap(this->get(i_max), this->get(parent));

                // Recursively heapify the parent node
                heapify(this->get(i_max), parent);
            }
        }
}

void Heap::buildHeap(Array& numbers)
{
    int n;
    for (int i = numbers / 2 - 1; i >= 0; i--)
            heapify(arr, numbers, i);
}

void Heap::heapSort()
{
    for (int i = n - 1; i > 0; i--) {
            // Move current root to end
            swap(arr[0], arr[i]);

            // call max heapify on the reduced heap
            heapify(arr, i, 0);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
